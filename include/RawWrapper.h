#ifndef RAWWRAPPER_H
#define RAWWRAPPER_H
#include <iostream>
#include "SerializationObjects.h"
#include "Logger.h"
struct offset
{
    int x;
    int y;
    int code;
    ///boost archive
    template <typename Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        ar & x;
        ar& y;
        ar&code;
    }
};
typedef unsigned short ushort;
typedef unsigned int uint;

template< typename T >
struct array_deleter
{
  void operator ()( T const * p)
  {
    delete[] p;
  }
};

class RawWrapper
{
public:
    virtual ~RawWrapper();
    virtual RawWrapper * copy()= 0;
    virtual int maxValue()=0;
    uint extend;
    uint height, width;
    virtual void cropToActiveArea()  = 0;
    virtual void doWhiteBalance()  = 0;
    bool isCropped=false;
    std::shared_ptr<ushort> extendedRawImage;
    //ushort * extendedRawImage=nullptr;
    ushort ** extendRawArray=nullptr;
    //virtual int open()  = 0;
    //white balance multipliers
    boost::array<float,4> multipliers;

    //key: color code @see define rules for color codes
    //values: offset in bayer filter
    boost::array<offset,4> colorOffsets;
    //key:order in bayer filter
    //value:color code @see define rules for color codes
    boost::array<int,4> bayerOffsets;
    boost::array<int,4> blackLevels;
    void free();
    InputData internalSettings;
    virtual boost::array<boost::array<double,3>,3> getColorArray()=0;
protected:
    RawWrapper();
    void setExtendValue();
    ushort * extendBayerArea(ushort* data);
    ushort * extendBayerArea(ushort* data, int extendBlocks);
    void buildArray();
    int getSize() const
    {
        return (height+(extend<<1))*(width+(extend<<1));
    }
    constexpr static double noConv[3][3] =
    {
        { 1.0, 0, 0},
        { 0, 1.0, 0 },
        { 0, 0, 1.0 }
    };
private:

    Logger<RawWrapper> logger = Logger<RawWrapper>::getInstance(this);

};

#endif // RAWWRAPPER_H
