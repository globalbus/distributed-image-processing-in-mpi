/***************************************************************
 * Name:      GUIProjectMain.cpp
 * Purpose:   Code for Application Frame
 * Author:     ()
 * Created:   2014-06-30
 * Copyright:  ()
 * License:
 **************************************************************/

#include "GUIProjectMain.h"
#include <wx/msgdlg.h>

//(*InternalHeaders(GUIProjectFrame)
#include <wx/intl.h>
#include <wx/string.h>
//*)
#include "AsioCommunication.h"

using namespace std;

//helper functions
enum wxbuildinfoformat {
    short_f, long_f };

wxString wxbuildinfo(wxbuildinfoformat format)
{
    wxString wxbuild(wxVERSION_STRING);

    if (format == long_f )
    {
#if defined(__WXMSW__)
        wxbuild << _T("-Windows");
#elif defined(__UNIX__)
        wxbuild << _T("-Linux");
#endif

#if wxUSE_UNICODE
        wxbuild << _T("-Unicode build");
#else
        wxbuild << _T("-ANSI build");
#endif // wxUSE_UNICODE
    }

    return wxbuild;
}

//(*IdInit(GUIProjectFrame)
const long GUIProjectFrame::ID_BUTTON1 = wxNewId();
const long GUIProjectFrame::ID_CHOICE2 = wxNewId();
const long GUIProjectFrame::ID_CHOICE3 = wxNewId();
const long GUIProjectFrame::ID_CHOICE1 = wxNewId();
const long GUIProjectFrame::ID_CHOICE4 = wxNewId();
const long GUIProjectFrame::ID_SPINCTRL3 = wxNewId();
const long GUIProjectFrame::ID_SPINCTRL4 = wxNewId();
const long GUIProjectFrame::ID_SPINCTRL2 = wxNewId();
const long GUIProjectFrame::ID_SPINCTRL1 = wxNewId();
const long GUIProjectFrame::ID_STATICBOX1 = wxNewId();
const long GUIProjectFrame::ID_CHOICE5 = wxNewId();
const long GUIProjectFrame::ID_MESSAGEDIALOG1 = wxNewId();
//*)

BEGIN_EVENT_TABLE(GUIProjectFrame,wxFrame)
    //(*EventTable(GUIProjectFrame)
    //*)
END_EVENT_TABLE()

GUIProjectFrame::GUIProjectFrame(wxWindow* parent,wxWindowID id)
{
    //(*Initialize(GUIProjectFrame)
    wxGridBagSizer* GridBagSizer1;

    Create(parent, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxDEFAULT_FRAME_STYLE, _T("wxID_ANY"));
    SetClientSize(wxSize(459,900));
    GridBagSizer1 = new wxGridBagSizer(wxDLG_UNIT(this,wxSize(10,0)).GetWidth(), wxDLG_UNIT(this,wxSize(10,0)).GetWidth());
    LoadDngButton = new wxButton(this, ID_BUTTON1, _("Load DNG"), wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_BUTTON1"));
    GridBagSizer1->Add(LoadDngButton, wxGBPosition(0, 0), wxDefaultSpan, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    Choice2 = new wxChoice(this, ID_CHOICE2, wxDefaultPosition, wxDefaultSize, 0, 0, 0, wxDefaultValidator, _T("ID_CHOICE2"));
    Choice2->SetSelection( Choice2->Append(_("None")) );
    Choice2->Append(_("Statistic Filtering"));
    Choice2->Append(_("Median Filtering"));
    GridBagSizer1->Add(Choice2, wxGBPosition(3, 0), wxDefaultSpan, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    Choice3 = new wxChoice(this, ID_CHOICE3, wxDefaultPosition, wxSize(253,27), 0, 0, 0, wxDefaultValidator, _T("ID_CHOICE3"));
    Choice3->SetSelection( Choice3->Append(_("None")) );
    Choice3->Append(_("Nonlinear filter"));
    Choice3->Append(_("Nonlinear filter with median correction"));
    Choice3->Append(_("Median filter"));
    GridBagSizer1->Add(Choice3, wxGBPosition(4, 0), wxDefaultSpan, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    Choice1 = new wxChoice(this, ID_CHOICE1, wxDefaultPosition, wxDefaultSize, 0, 0, 0, wxDefaultValidator, _T("ID_CHOICE1"));
    Choice1->SetSelection( Choice1->Append(_("Nearest Neighbor")) );
    Choice1->Append(_("Bilinear"));
    Choice1->Append(_("Simple Gradients"));
    Choice1->Append(_("VNG  Demosaic"));
    GridBagSizer1->Add(Choice1, wxGBPosition(2, 0), wxDefaultSpan, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    Choice4 = new wxChoice(this, ID_CHOICE4, wxDefaultPosition, wxDefaultSize, 0, 0, 0, wxDefaultValidator, _T("ID_CHOICE4"));
    Choice4->SetSelection( Choice4->Append(_("None")) );
    Choice4->Append(_("Dcraw default"));
    GridBagSizer1->Add(Choice4, wxGBPosition(5, 0), wxDefaultSpan, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    SpinCtrl3 = new wxSpinCtrl(this, ID_SPINCTRL3, _T("5"), wxDefaultPosition, wxDefaultSize, 0, 1, 100, 5, _T("ID_SPINCTRL3"));
    SpinCtrl3->SetValue(_T("5"));
    SpinCtrl3->SetToolTip(_("select prefilter mask, where n is odd number"));
    GridBagSizer1->Add(SpinCtrl3, wxGBPosition(8, 0), wxDefaultSpan, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    SpinCtrl4 = new wxSpinCtrl(this, ID_SPINCTRL4, _T("7"), wxDefaultPosition, wxDefaultSize, 0, 1, 100, 7, _T("ID_SPINCTRL4"));
    SpinCtrl4->SetValue(_T("7"));
    SpinCtrl4->SetToolTip(_("select postfilter mask, where n is odd number"));
    GridBagSizer1->Add(SpinCtrl4, wxGBPosition(9, 0), wxDefaultSpan, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    SpinCtrl2 = new wxSpinCtrl(this, ID_SPINCTRL2, _T("0"), wxDefaultPosition, wxDefaultSize, 0, -100, 100, 0, _T("ID_SPINCTRL2"));
    SpinCtrl2->SetValue(_T("0"));
    SpinCtrl2->SetToolTip(_("arbitary set saturation level value, where n is unsigned integer"));
    GridBagSizer1->Add(SpinCtrl2, wxGBPosition(7, 0), wxDefaultSpan, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    SpinCtrl1 = new wxSpinCtrl(this, ID_SPINCTRL1, _T("0"), wxDefaultPosition, wxDefaultSize, 0, -100, 100, 0, _T("ID_SPINCTRL1"));
    SpinCtrl1->SetValue(_T("0"));
    SpinCtrl1->SetToolTip(_("arbitary set black level value, where n is unsigned integer"));
    GridBagSizer1->Add(SpinCtrl1, wxGBPosition(6, 0), wxDefaultSpan, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    StaticBox1 = new wxStaticBox(this, ID_STATICBOX1, _("Processed PNG"), wxDefaultPosition, wxSize(192,60), 0, _T("ID_STATICBOX1"));
    GridBagSizer1->Add(StaticBox1, wxGBPosition(1, 0), wxDefaultSpan, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    Choice5 = new wxChoice(this, ID_CHOICE5, wxDefaultPosition, wxDefaultSize, 0, 0, 0, wxDefaultValidator, _T("ID_CHOICE5"));
    Choice5->SetSelection( Choice5->Append(_("8 bit")) );
    Choice5->Append(_("16 bit"));
    Choice5->SetToolTip(_("select output image precision, possible values 8 and 16 bit per channel"));
    GridBagSizer1->Add(Choice5, wxGBPosition(10, 0), wxDefaultSpan, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    SetSizer(GridBagSizer1);
    FileDialog1 = new wxFileDialog(this, _("Select file"), wxEmptyString, wxEmptyString, _("DNG Files|*.dng;*.DNG"), wxFD_DEFAULT_STYLE, wxDefaultPosition, wxDefaultSize, _T("wxFileDialog"));
    MessageDialog1 = new wxMessageDialog(this, wxEmptyString, _("Done!"), wxOK|wxCANCEL, wxDefaultPosition);
    SetSizer(GridBagSizer1);
    Layout();

    Connect(ID_BUTTON1,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&GUIProjectFrame::OnloadDngButtonClick);
    //*)
}

GUIProjectFrame::~GUIProjectFrame()
{
    //(*Destroy(GUIProjectFrame)
    //*)
}

void GUIProjectFrame::OnQuit(wxCommandEvent& event)
{
    Close();
}



void GUIProjectFrame::OnloadDngButtonClick(wxCommandEvent& event)
{
    string address;
    int d = FileDialog1->ShowModal();
    //if(d==wxOK) {
        std::ifstream file(FileDialog1->GetPath().char_str(), std::ios::binary);
    // read the data:
        dngBuffer= std::vector<char>((std::istreambuf_iterator<char>(file)),
                              std::istreambuf_iterator<char>());

        int demosaic,colorMatrix, prefilter, postfilter;
        int black,saturation,prefilterMask,postfilterMask,precision;
        demosaic=getDemosaic(Choice1);
        colorMatrix=getColorMatrix(Choice2);
        prefilter=getPrefilter(Choice3);
        postfilter=getPostfilter(Choice4);
        black=SpinCtrl1->GetValue();
        saturation = SpinCtrl2->GetValue();
        prefilterMask=SpinCtrl3->GetValue();
        postfilterMask=SpinCtrl4->GetValue();
        precision=getPrecision(Choice5);
        InputData inputSettings;
        inputSettings.buffer=dngBuffer;
        inputSettings.postfilterMask=postfilterMask;
        inputSettings.postfilter=static_cast<postfilterType>(postfilter);
        inputSettings.prefilterMask=prefilterMask;
        inputSettings.prefilter=static_cast<prefilterType>(prefilter);
        inputSettings.blackLevel=black;
        inputSettings.customBlackLevel=true;
        inputSettings.colorMatrix=static_cast<colorMatrixType>(colorMatrix);
        inputSettings.demosaic=static_cast<demosaicMethod>(demosaic);
        inputSettings.saturationLevel=saturation;
        inputSettings.customSaturationLevel=true;
        inputSettings.precision=precision;
        string s="";
        for(int i=0;i<10;i++) {
            std::cout<<(int)inputSettings.buffer[i]<<std::endl;
            s+=(int)inputSettings.buffer[i];
        }
        //MessageDialog1->SetMessage(s);
        MessageDialog1->ShowModal();
    //@COPYPASTA!!!!
        AsioCommunication comm;
            comm.connectAsClient(address);
            comm.sendObject(inputSettings, messageType::inputData);
            cout<<"send"<<endl;
            Header m=comm.readHeader();
            uint32_t id=m.value;
            cout<<id<<endl;
            m=comm.readHeader();
            comm.finishedId(id);
            cout<<"query"<<endl;
            m=comm.readHeader();

            OutputData out = comm.readObject<OutputData>(m.value);
            cout<<"read"<<endl;
            /*
            PngReaderWriter writer;
            WorkingImage<arrayType, 3> image(out.realWidth, out.realHeight);
            //cout<<out.realHeight<<" "<<out.realWidth<<endl;
            //cout<<out.startingHeight<<" "<<out.startingWidth<<endl;
            image.fill(0,0,0);
            int cumulativeSize=0;
            while(true)
            {
                cout<<out.realHeight<<" "<<out.realWidth<<endl;
                cout<<out.startingHeight<<" "<<out.startingWidth<<endl;
                pair<int, int> WH = writer.readPngFromMemory(image, out);
                cumulativeSize+=WH.first*WH.second;
                if(cumulativeSize==out.realHeight*out.realWidth)
                    break;
                m=comm.readHeader();
                out = comm.readObject<OutputData>(m.value);
            }
            buffer = writer.writePngToMemory(image, image.getPrecision());
            ofstream os ("test.png", ofstream::binary);
            if (os)
            {
                os.write(buffer.data(), buffer.size());
            }
            */
}
int GUIProjectFrame::getDemosaic(wxChoice* Choice1) {

            return Choice1->GetSelection();

}
int GUIProjectFrame::getColorMatrix(wxChoice* Choice1) {

            return Choice1->GetSelection();
}
int GUIProjectFrame::getPrefilter(wxChoice* Choice1) {

            return Choice1->GetSelection();
}
int GUIProjectFrame::getPostfilter(wxChoice* Choice1) {

            return Choice1->GetSelection();
}
int GUIProjectFrame::getPrecision(wxChoice* Choice1) {

            return Choice1->GetSelection();
}
