#include "PartialRawWrapper.h"

PartialRawWrapper::PartialRawWrapper()
{
    //ctor
}

PartialRawWrapper::~PartialRawWrapper()
{
    //dtor
}
RawWrapper* PartialRawWrapper::copy()
{
    PartialRawWrapper * wrapper = new PartialRawWrapper(*this);
    wrapper->getCopy(*this);
    wrapper->buildArray();
    return wrapper;
}
boost::array<boost::array<double,3>,3> PartialRawWrapper::getColorArray()
{
    return matrix;
}
void PartialRawWrapper::doWhiteBalance()
{

}
int PartialRawWrapper::maxValue()
{
    return maxValueConst;
}
void PartialRawWrapper::cropToActiveArea()
{
    buildArray();
}
std::vector<PartialRawWrapper> PartialRawWrapper::split(RawWrapper& wrapper, int howMany)
{
    Splitter splits;
    splits.wrapper=&wrapper;
    splits.splitter(wrapper.width, wrapper.height, 0,0, howMany);
    return splits.parts;
}
void Splitter::splitter(int width, int height, int startingx, int startingy, int howMany)
{
    const static int chunkSize=2;
    if(howMany==2)
    {
        if(height>width)
        {
            int newHeight=height/2;
            newHeight=newHeight-newHeight%chunkSize;
            PartialRawWrapper part1;
            part1.getCopy(*wrapper, width, newHeight, startingx, startingy);
            parts.push_back(part1);
            PartialRawWrapper part2;
            part2.getCopy(*wrapper, width, height-newHeight, startingx+newHeight, startingy);
            parts.push_back(part2);
        }
        else
        {
            int newWidth=width/2;
            newWidth=newWidth-newWidth%chunkSize;
            PartialRawWrapper part1;
            part1.getCopy(*wrapper, newWidth, height, startingx, startingy);
            parts.push_back(part1);
            PartialRawWrapper part2;
            part2.getCopy(*wrapper, width-newWidth, height, startingx, startingy+newWidth);
            parts.push_back(part2);
        }
        return;
    }
    if(howMany==3)
    {
        if(height>width)
        {
            int newHeight=height/3;
            newHeight=newHeight-newHeight%chunkSize;
            PartialRawWrapper part1;
            part1.getCopy(*wrapper, width, newHeight, startingx, startingy);
            parts.push_back(part1);
            PartialRawWrapper part2;
            part2.getCopy(*wrapper, width, newHeight, startingx+newHeight, startingy);
            parts.push_back(part2);
            PartialRawWrapper part3;
            part3.getCopy(*wrapper, width, height-2*newHeight,  startingx+2*newHeight, startingy);
            parts.push_back(part3);
        }
        else
        {
            int newWidth=width/3;
            newWidth=newWidth-newWidth%chunkSize;
            PartialRawWrapper part1;
            part1.getCopy(*wrapper, newWidth, height,  startingx, startingy);
            parts.push_back(part1);
            PartialRawWrapper part2;
            part2.getCopy(*wrapper, newWidth, height, startingx, startingy+newWidth);
            parts.push_back(part2);
            PartialRawWrapper part3;
            part3.getCopy(*wrapper, width-2*newWidth, height, startingx, startingy+2*newWidth);
            parts.push_back(part3);
        }
        return;
    }
    int d=howMany/2;
    int r=howMany%2;

    if(height>width)
    {
        int newHeight=height/2;
        newHeight=newHeight-newHeight%chunkSize;
        int offsHeight=height-newHeight*2;
        splitter(width,newHeight, startingx,startingy, d);
        splitter(width, newHeight+offsHeight, startingx+newHeight,startingy, d+r);
    }
    else
    {
        int newWidth=width/2;
        newWidth=newWidth-newWidth%chunkSize;
        int offsWidth= width-newWidth*2;
        splitter(newWidth,height,startingx,startingy, d);
        splitter(newWidth+offsWidth,height,startingx,startingy+newWidth, d+r);
    }


}
void PartialRawWrapper::getCopy(RawWrapper & wrapper)
{
    this->height=wrapper.height;
    this->width=wrapper.width;
    this->extend=wrapper.extend;
    this->multipliers=wrapper.multipliers;
    this->internalSettings=wrapper.internalSettings;
    this->bayerOffsets=wrapper.bayerOffsets;
    this->blackLevels=wrapper.blackLevels;
    this->colorOffsets=wrapper.colorOffsets;
    this->maxValueConst=wrapper.maxValue();
    this->matrix=wrapper.getColorArray();
    shared_ptr<ushort> ptr(new ushort[getSize()], array_deleter<ushort>());
    extendedRawImage=ptr;
    std::copy(wrapper.extendedRawImage.get(), wrapper.extendedRawImage.get()+getSize(), extendedRawImage.get());
}
void PartialRawWrapper::getCopy(RawWrapper & wrapper, int width, int height, int startingx, int startingy)
{
    this->height=height;
    this->width=width;
    this->extend=wrapper.extend;
    this->multipliers=wrapper.multipliers;
    this->internalSettings=wrapper.internalSettings;
    this->bayerOffsets=wrapper.bayerOffsets;
    this->blackLevels=wrapper.blackLevels;
    this->colorOffsets=wrapper.colorOffsets;
    this->maxValueConst=wrapper.maxValue();
    this->matrix=wrapper.getColorArray();
    this->startingx=startingx;
    this->startingy=startingy;
    //extendedRawImage= new unsigned short [getSize()];
    shared_ptr<ushort> ptr(new ushort[getSize()], array_deleter<ushort>());
    extendedRawImage=ptr;
    ushort * dest = extendedRawImage.get();
    ushort * src =wrapper.extendedRawImage.get();
    for(uint i=0; i<height+(extend<<1); i++)
    {
        int singleRow=(width+(extend<<1));
        int offsetSource=(startingx+i)*(wrapper.width+(extend<<1))+startingy;
        int offsetDest=i*singleRow;
        std::copy(src+offsetSource, src+offsetSource+singleRow, dest+offsetDest);
    }
}
