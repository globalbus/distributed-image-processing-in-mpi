#include "RawImage.h"
#include <iostream>
#include <cmath>
#include <algorithm>
#include <cstring>
#include <fstream>
#include <climits>
using namespace std;

//redcodes, defined with LibRAW convention
#define redCode 0
#define green1Code 1
#define blueCode 2
#define green2Code 3

#define offsetRed colorOffsets[redCode]
#define offsetGreen1 colorOffsets[green1Code]
#define offsetBlue colorOffsets[blueCode]
#define offsetGreen2 colorOffsets[green2Code]

RAWImage::RAWImage(RawWrapper * sourceWrapper) {
	this->sourceWrapper = sourceWrapper;
	this->inputSettings = sourceWrapper->internalSettings;
}

void RAWImage::copyForProcess() {
#define colorSize 3
	if (!blocked) {
		blocked = true;
		//sourceWrapper->cropToActiveArea();
		height = sourceWrapper->height;
		width = sourceWrapper->width;
		extend = sourceWrapper->extend;
		extendedRawImage = sourceWrapper->extendedRawImage.get();
		extendRawArray = sourceWrapper->extendRawArray;
		bayerOffsets = sourceWrapper->bayerOffsets;
		colorOffsets = sourceWrapper->colorOffsets;
		boost::array<int,4> blackLevels = sourceWrapper->blackLevels;

		image = new WorkingImage<arrayType, colorSize>(width, height);
		imageSize = height * width * colorSize;

		singleMemoryBlock = image->singleMemoryBlock;

		memset(image->singleMemoryBlock, 0, sizeof(arrayType) * imageSize);
		boost::array<float, 4> multipliers = sourceWrapper->multipliers;
		for (int j = 0, k, l = 0; j < height + (extend << 1); j++) {
			for (int i = 0; i < width + (extend << 1); i++) {
				k = bayerFind(j, i);
				//substract average black current
				if (extendedRawImage[l] < blackLevels[k]) //fail-safe
					extendedRawImage[l] = 0;
				else
					extendedRawImage[l] -= blackLevels[k];

				extendedRawImage[l] *= multipliers[k];
				l++;
			}
		}
		if (inputSettings.verbose)
            logger.log("image balanced and copied");
		blocked = false;
	}
}
void RAWImage::simpleGradientsDemosaic() {
	if (!blocked) {
		blocked = true;
		int k;
		int gX, gY, temp;

		//first run - green values
		for (int j = 0; j < height; j++) {
			for (int i = 0; i < width; i++) {
				k = bayerFind(j, i);
				//if(k==redCode||k==blueCode)//red and blue
				if (k & 1) {
					image->setG1(j, i, extendRawArray[j][i]);
				} else {
					gX = abs(
							extendRawArray[j + 1][i]
									- extendRawArray[j - 1][i]);
					gY = abs(
							extendRawArray[j][i + 1]
									- extendRawArray[j][i - 1]);
					if (gX > gY)
						image->setG1(j, i,
								(extendRawArray[j][i + 1]
										+ extendRawArray[j][i - 1]) >> 1);
					else if (gX < gY)
						image->setG1(j, i,
								(extendRawArray[j + 1][i]
										+ extendRawArray[j - 1][i]) >> 1);
					else
						image->setG1(j, i,
								(extendRawArray[j + 1][i]
										+ extendRawArray[j - 1][i]
										+ extendRawArray[j][i + 1]
										+ extendRawArray[j][i - 1]) >> 2);
				}
			}
		}
		WorkingImage<arrayType, colorSize> * imageCopy =
				image->extendWorkingImage(1);
		//second run - blue and red
		for (int j = 0; j < height; j++) {
			for (int i = 0; i < width; i++) {
				switch (bayerFind(j, i)) {
				case redCode:
					//red at red
					image->setR(j, i, extendRawArray[j][i]);
					//blue at red
					temp = image->getG1(j, i)
							+ ((extendRawArray[j + 1][i + 1]
									- imageCopy->getG1(j + 1, i + 1)
									+ extendRawArray[j - 1][i + 1]
									- imageCopy->getG1(j - 1, i + 1)
									+ extendRawArray[j + 1][i - 1]
									- imageCopy->getG1(j + 1, i - 1)
									+ extendRawArray[j - 1][i - 1]
									- imageCopy->getG1(j - 1, i - 1)) / 4);

					image->checkedSetB(j, i, temp);
					break;
				case green1Code:
					//red at green1
					temp = image->getG1(j, i)
							+ ((extendRawArray[j][i + 1]
									- imageCopy->getG1(j, i + 1)
									+ extendRawArray[j][i - 1]
									- imageCopy->getG1(j, i - 1)) / 2);
					image->checkedSetR(j, i, temp);
					//blue at green1
					temp = image->getG1(j, i)
							+ ((extendRawArray[j + 1][i]
									- imageCopy->getG1(j + 1, i)
									+ extendRawArray[j - 1][i]
									- imageCopy->getG1(j - 1, i)) / 2);
					image->checkedSetB(j, i, temp);
					break;
				case blueCode:
					//blue at blue
					image->setB(j, i, extendRawArray[j][i]);
					//red at blue
					temp = image->getG1(j, i)
							+ ((extendRawArray[j + 1][i + 1]
									- imageCopy->getG1(j + 1, i + 1)
									+ extendRawArray[j - 1][i + 1]
									- imageCopy->getG1(j - 1, i + 1)
									+ extendRawArray[j + 1][i - 1]
									- imageCopy->getG1(j + 1, i - 1)
									+ extendRawArray[j - 1][i - 1]
									- imageCopy->getG1(j - 1, i - 1)) / 4);
					image->checkedSetR(j, i, temp);
					break;
				case green2Code:
					//red at green2
					temp = image->getG1(j, i)
							+ ((extendRawArray[j + 1][i]
									- imageCopy->getG1(j + 1, i)
									+ extendRawArray[j - 1][i]
									- imageCopy->getG1(j - 1, i)) / 2);
					image->checkedSetR(j, i, temp);
					//blue at green2
					temp = image->getG1(j, i)
							+ ((extendRawArray[j][i + 1]
									- imageCopy->getG1(j, i + 1)
									+ extendRawArray[j][i + 1]
									- imageCopy->getG1(j, i - 1)) / 2);
					image->checkedSetB(j, i, temp);
					break;
				}
			}
		}
		delete imageCopy;
		if (inputSettings.verbose)
            logger.log("simple gradients demosaicing applied");
		blocked = false;
	}
}

void RAWImage::VNGDemosaic() {
	if (!blocked) {
		blocked = true;
		int gradients[8];
		int k, threshold;
		int temp5;
		for (int j = 0; j < height; j++) {
			for (int i = 0; i < width; i++) {
				int sumK = 0, temp2 = 0, temp4 = 0, temp3 = 0;
				int gradientCount = 0;
				k = bayerFind(j, i);
				//N
				// 0 1 2 1 0
				// 0 1 2 1 0
				// 0 1 2 1 0
				// 0 1 2 1 0
				// 0 0 0 0 0
				//G8-G18 R3-R13
				//B7-B17 B9-B19
				//G2-G12 G4-G14
				gradients[0] = abs(
						extendRawArray[j - 1][i] - extendRawArray[j + 1][i])
						+ abs(extendRawArray[j - 2][i] - extendRawArray[j][i])
						+ (abs(
								extendRawArray[j - 1][i - 1]
										- extendRawArray[j + 1][i - 1])
								+ abs(
										extendRawArray[j - 1][i + 1]
												- extendRawArray[j + 1][i + 1])
								+ abs(
										extendRawArray[j - 2][i - 1]
												- extendRawArray[j][i - 1])
								+ abs(
										extendRawArray[j - 2][i + 1]
												- extendRawArray[j][i + 1]))
								/ 2;
				//E
				// 0 0 0 0 0
				// 0 1 1 1 1
				// 0 2 2 2 2
				// 0 1 1 1 1
				// 0 0 0 0 0
				//G14-G12 R15-R13
				//B9-B7 B19-B17
				//G10-G8 G20-G18
				gradients[1] = abs(
						extendRawArray[j][i + 1] - extendRawArray[j][i - 1])
						+ abs(extendRawArray[j][i + 2] - extendRawArray[j][i])
						+ (abs(
								extendRawArray[j - 1][i + 1]
										- extendRawArray[j - 1][i - 1])
								+ abs(
										extendRawArray[j + 1][i + 1]
												- extendRawArray[j + 1][i - 1])
								+ abs(
										extendRawArray[j - 1][i + 2]
												- extendRawArray[j - 1][i])
								+ abs(
										extendRawArray[j + 1][i + 2]
												- extendRawArray[j + 1][i]))
								/ 2;
				//S
				// 0 0 0 0 0
				// 0 1 2 1 0
				// 0 1 2 1 0
				// 0 1 2 1 0
				// 0 1 2 1 0
				//G18-G8 R23-R13
				//B17-B7 B19-B9
				//G22-G12 G24-G14
				gradients[2] = abs(
						extendRawArray[j + 1][i] - extendRawArray[j - 1][i])
						+ abs(extendRawArray[j + 2][i] - extendRawArray[j][i])
						+ (abs(
								extendRawArray[j + 1][i - 1]
										- extendRawArray[j - 1][i - 1])
								+ abs(
										extendRawArray[j + 1][i + 1]
												- extendRawArray[j - 1][i + 1])
								+ abs(
										extendRawArray[j + 2][i - 1]
												- extendRawArray[j][i - 1])
								+ abs(
										extendRawArray[j + 2][i + 1]
												- extendRawArray[j][i + 1]))
								/ 2;
				//W
				// 0 0 0 0 0
				// 1 1 1 1 0
				// 2 2 2 2 0
				// 1 1 1 1 0
				// 0 0 0 0 0
				//G14-G12 R11-R13
				//B7-B9 B17-B19
				//G6-G8 G16-G18
				gradients[3] = abs(
						extendRawArray[j][i + 1] - extendRawArray[j][i - 1])
						+ abs(extendRawArray[j][i - 2] - extendRawArray[j][i])
						+ (abs(
								extendRawArray[j - 1][i - 1]
										- extendRawArray[j - 1][i + 1])
								+ abs(
										extendRawArray[j + 1][i - 1]
												- extendRawArray[j + 1][i + 1])
								+ abs(
										extendRawArray[j - 1][i - 2]
												- extendRawArray[j - 1][i])
								+ abs(
										extendRawArray[j + 1][i - 2]
												- extendRawArray[j + 1][i]))
								/ 2;
				//NE
				// 0 0 0 1 2
				// 0 0 1 2 1
				// 0 1 2 1 0
				// 0 2 1 0 0
				// 0 0 0 0 0
				//B9-B17 R5-R13
				//G8-G12 G14-G18
				//G4-G8 G10-G14
//            gradients[4]=abs(extendRawArray[j-1][i+1]-extendRawArray[j+1][i-1])+abs(extendRawArray[j-2][i+2]-extendRawArray[j][i])
//                         +(abs(extendRawArray[j-1][i]-extendRawArray[j][i-1])+abs(extendRawArray[j][i+1]-extendRawArray[j+1][i])
//                           +abs(extendRawArray[j-2][i+1]-extendRawArray[j-1][i])+abs(extendRawArray[j-1][i+2]-extendRawArray[j][i+1]))/2;
				gradients[4] = abs(
						extendRawArray[j - 1][i + 1]
								- extendRawArray[j + 1][i - 1])
						+ abs(
								extendRawArray[j - 2][i + 2]
										- extendRawArray[j][i])
						+ abs(
								extendRawArray[j - 2][i + 1]
										- extendRawArray[j][i - 1])
						+ abs(
								extendRawArray[j - 1][i + 2]
										- extendRawArray[j + 1][i]);
				//SE
				// 0 0 0 0 0
				// 0 2 1 0 0
				// 0 1 2 1 0
				// 0 0 1 2 1
				// 0 0 0 1 2
				//B19-B7 R25-R13
				//G14-G8 G18-G12
				//G20-G14 G24-G18
//            gradients[5]=abs(extendRawArray[j+1][i+1]-extendRawArray[j-1][i-1])+abs(extendRawArray[j+2][i+2]-extendRawArray[j][i])
//                         +(abs(extendRawArray[j][i+1]-extendRawArray[j-1][i])+abs(extendRawArray[j+1][i]-extendRawArray[j][i-1])
//                           +abs(extendRawArray[j+1][i+2]-extendRawArray[j][i+1])+abs(extendRawArray[j+2][i+1]-extendRawArray[j+1][i]))/2;
				gradients[5] = abs(
						extendRawArray[j + 1][i + 1]
								- extendRawArray[j - 1][i - 1])
						+ abs(
								extendRawArray[j + 2][i + 2]
										- extendRawArray[j][i])
						+ abs(
								extendRawArray[j + 1][i + 2]
										- extendRawArray[j - 1][i])
						+ abs(
								extendRawArray[j + 2][i + 1]
										- extendRawArray[j][i - 1]);
				//NW
				// 2 1 0 0 0
				// 1 2 1 0 0
				// 0 1 2 1 0
				// 0 0 1 2 0
				// 0 0 0 0 0
				//B19-B7 R1-R13
				//G14-G8 G18-G12
				//G6-G12 G2-G8
//            gradients[6]=abs(extendRawArray[j+1][i+1]-extendRawArray[j-1][i-1])+abs(extendRawArray[j-2][i-2]-extendRawArray[j][i])
//                         +(abs(extendRawArray[j][i+1]-extendRawArray[j-1][i])+abs(extendRawArray[j+1][i]-extendRawArray[j][i-1])
//                           +abs(extendRawArray[j-1][i-2]-extendRawArray[j][i-1])+abs(extendRawArray[j-2][i-1]-extendRawArray[j-1][i]))/2;
				gradients[6] = abs(
						extendRawArray[j + 1][i + 1]
								- extendRawArray[j - 1][i - 1])
						+ abs(
								extendRawArray[j - 2][i - 2]
										- extendRawArray[j][i])
						+ abs(
								extendRawArray[j - 1][i - 2]
										- extendRawArray[j + 1][i])
						+ abs(
								extendRawArray[j - 2][i - 1]
										- extendRawArray[j][i + 1]);
				//SW
				// 0 0 0 0 0
				// 0 0 1 2 0
				// 0 1 2 1 0
				// 1 2 1 0 0
				// 2 1 0 0 0
				//B9-B17 R21-R13
				//G8-G12 G14-G18
				//G22-G18 G16-G12
//            gradients[7]=abs(extendRawArray[j-1][i+1]-extendRawArray[j+1][i-1])+abs(extendRawArray[j+2][i-2]-extendRawArray[j][i])
//                         +(abs(extendRawArray[j-1][i]-extendRawArray[j][i-1])+abs(extendRawArray[j][i+1]-extendRawArray[j+1][i])
//                           +abs(extendRawArray[j+2][i-1]-extendRawArray[j+1][i])+abs(extendRawArray[j+1][i-2]-extendRawArray[j][i-1]))/2;
				gradients[7] = abs(
						extendRawArray[j - 1][i + 1]
								- extendRawArray[j + 1][i - 1])
						+ abs(
								extendRawArray[j + 2][i - 2]
										- extendRawArray[j][i])
						+ abs(
								extendRawArray[j + 2][i - 1]
										- extendRawArray[j][i + 1])
						+ abs(
								extendRawArray[j + 1][i - 2]
										- extendRawArray[j - 1][i]);

				pair<int*, int*> minMax = minmax_element(gradients,
						gradients + 8);

				threshold = round(
						1.5 * *minMax.first
								+ 0.5 * (*minMax.second - *minMax.first));

				//N
				if (gradients[0] < threshold) {
					gradientCount++;
					temp4 += extendRawArray[j - 1][i]; //sure value G2 B G1 R
					sumK += (extendRawArray[j - 2][i] + extendRawArray[j][i])
							>> 1; //the same color as k = sumK
					temp2 += (extendRawArray[j - 1][i - 1]
							+ extendRawArray[j - 1][i + 1]) >> 1; //B G2 R G1
					temp3 += (extendRawArray[j - 2][i - 1]
							+ extendRawArray[j - 2][i + 1]) >> 1; //G1 R G2 B

				}
//            //E
				if (gradients[1] < threshold) {
					gradientCount++;
					temp3 += extendRawArray[j][i + 1]; //sure value G1 R G2 B
					sumK += (extendRawArray[j][i + 2] + extendRawArray[j][i])
							>> 1; //the same color as k = sumK
					temp2 += (extendRawArray[j - 1][i + 1]
							+ extendRawArray[j + 1][i + 1]) >> 1; //B G2 R G1
					temp4 += (extendRawArray[j - 1][i + 2]
							+ extendRawArray[j + 1][i + 2]) >> 1; //G2 B G1 R
				}
				//S
				if (gradients[2] < threshold) {
					gradientCount++;
					temp4 += extendRawArray[j + 1][i]; //sure value G2 B G1 R
					sumK += (extendRawArray[j + 2][i] + extendRawArray[j][i])
							>> 1; //the same color as k = sumK
					temp2 += (extendRawArray[j + 1][i - 1]
							+ extendRawArray[j + 1][i + 1]) >> 1; //B G2 R G1
					temp3 += (extendRawArray[j + 2][i - 1]
							+ extendRawArray[j + 2][i + 1]) >> 1; //G1 R G2 B
				}
				//W
				if (gradients[3] < threshold) {
					gradientCount++;
					temp3 += extendRawArray[j][i - 1]; //sure value G1 R G2 B
					sumK += (extendRawArray[j][i - 2] + extendRawArray[j][i])
							>> 1; //the same color as k = sumK
					temp2 += (extendRawArray[j - 1][i - 1]
							+ extendRawArray[j + 1][i - 1]) >> 1; //B G2 R G1
					temp4 += (extendRawArray[j - 1][i - 2]
							+ extendRawArray[j + 1][i - 2]) >> 1; //G2 B G1 R
				}
				//NE
				if (gradients[4] < threshold) {
					gradientCount++;
					temp3 += (extendRawArray[j - 2][i + 1]
							+ extendRawArray[j][i + 1]) >> 1; //G1 R G2 B;
					sumK +=
							(extendRawArray[j - 2][i + 2] + extendRawArray[j][i])
									>> 1; //the same color as k = sumK
					temp2 += extendRawArray[j - 1][i + 1]; //sure value B G2 R G1
					temp4 += (extendRawArray[j - 1][i + 2]
							+ extendRawArray[j - 1][i]) >> 1; //G2 B G1 R

				}
				//SE
				if (gradients[5] < threshold) {
					gradientCount++;
					temp3 += (extendRawArray[j + 2][i + 1]
							+ extendRawArray[j][i + 1]) >> 1; //G1 R G2 B
					sumK +=
							(extendRawArray[j + 2][i + 2] + extendRawArray[j][i])
									>> 1; //the same color as k = sumK
					temp2 += extendRawArray[j + 1][i + 1]; //sure value B G2 R G1
					temp4 += (extendRawArray[j + 1][i + 2]
							+ extendRawArray[j + 1][i]) >> 1; //G2 B G1 R
				}
				//NW
				if (gradients[6] < threshold) {
					gradientCount++;
					temp3 += (extendRawArray[j - 2][i - 1]
							+ extendRawArray[j][i - 1]) >> 1; //G1 R G2 B
					sumK +=
							(extendRawArray[j - 2][i - 2] + extendRawArray[j][i])
									>> 1; //the same color as k = sumK
					temp2 += extendRawArray[j - 1][i - 1]; //sure value B G2 R G1
					temp4 += (extendRawArray[j - 1][i - 2]
							+ extendRawArray[j - 1][i]) >> 1; //G2 B G1 R
				}
				//SW
				if (gradients[7] < threshold) {
					gradientCount++;
					temp3 += (extendRawArray[j + 2][i - 1]
							+ extendRawArray[j][i - 1]) >> 1; //G1 R G2 B
					sumK +=
							(extendRawArray[j + 2][i - 2] + extendRawArray[j][i])
									>> 1; //the same color as k = sumK
					temp2 += extendRawArray[j + 1][i - 1]; //sure value B G2 R G1
					temp4 += (extendRawArray[j + 1][i - 2]
							+ extendRawArray[j + 1][i]) >> 1; //G2 B G1 R
				}
				if (gradientCount == 0) //gradient can be 0 at all elements, we need to check it.
						{
					image->setR(j, i,
							extendRawArray[j - colorOffsets[k].x + offsetRed.x][i-colorOffsets[k].y+offsetRed.y]);
					image->setG1(j,i,extendRawArray[j-colorOffsets[k].x+offsetGreen1.x][i-colorOffsets[k].y+offsetGreen1.y]);
					image->setB(j, i, extendRawArray[j-colorOffsets[k].x+offsetBlue.x][i-colorOffsets[k].y+offsetBlue.y]);
					continue;
				}
				switch (k) {
				case redCode:
					image->setR(j, i, extendRawArray[j][i]);
					//for 3 color array
					temp3 = (temp3 + temp4) >> 1;
					temp5 = extendRawArray[j][i]
							+ (temp3 - sumK) / gradientCount;
					image->checkedSetG1(j, i, temp5);

					temp5 = extendRawArray[j][i]
							+ (temp2 - sumK) / gradientCount;
					image->checkedSetB(j, i, temp5);

					break;
				case green1Code:
					//for 3 color array
					sumK = (sumK + temp2) >> 1;

					temp5 = extendRawArray[j][i]
							+ (temp3 - sumK) / gradientCount;
					image->checkedSetR(j, i, temp5);

					image->setG1(j, i, extendRawArray[j][i]);

					temp5 = extendRawArray[j][i]
							+ (temp4 - sumK) / gradientCount;
					image->checkedSetB(j, i, temp5);

					break;
				case blueCode:
					//for 3 color array
					temp3 = (temp3 + temp4) >> 1;
					temp5 = extendRawArray[j][i]
							+ (temp2 - sumK) / gradientCount;
					image->checkedSetR(j, i, temp5);

					temp5 = extendRawArray[j][i]
							+ (temp3 - sumK) / gradientCount;
					image->checkedSetG1(j, i, temp5);

					image->setB(j, i, extendRawArray[j][i]);

					break;
				case green2Code:
					//for 3 color array
					sumK = (sumK + temp2) >> 1;
					temp5 = extendRawArray[j][i]
							+ (temp4 - sumK) / gradientCount;
					image->checkedSetR(j, i, temp5);

					image->setG1(j, i, extendRawArray[j][i]);

					temp5 = extendRawArray[j][i]
							+ (temp3 - sumK) / gradientCount;
					image->checkedSetB(j, i, temp5);

					break;
				}
			}
		}
		if (inputSettings.verbose)
            logger.log("VNG demosaicing applied");
		blocked = false;
	}
}
void RAWImage::bilinearDemosaic_fast() {
	if (!blocked) {
		blocked = true;
		for (int j = 0; j < height; j++) {
			for (int i = 0; i < width; i++) {
				switch (bayerFind(j, i)) {
				case redCode:
					//red at red
					image->setR(j, i, extendRawArray[j][i]);
					//green at red
					image->setG1(j, i,
							(extendRawArray[j + 1][i] + extendRawArray[j][i + 1]
									+ extendRawArray[j - 1][i]
									+ extendRawArray[j][i - 1]) >> 2);
					//blue at red
					image->setB(j, i,
							(extendRawArray[j + 1][i + 1]
									+ extendRawArray[j - 1][i + 1]
									+ extendRawArray[j + 1][i - 1]
									+ extendRawArray[j - 1][i - 1]) >> 2);
					break;
				case green1Code:
					//red at green1
					image->setR(j, i,
							(extendRawArray[j][i + 1] + extendRawArray[j][i - 1])
									>> 1);
					//green1 at green1
					image->setG1(j, i, extendRawArray[j][i]);
					//blue at green1
					image->setB(j, i,
							(extendRawArray[j + 1][i] + extendRawArray[j - 1][i])
									>> 1);
					break;
				case blueCode:
					//red at blue
					image->setR(j, i,
							(extendRawArray[j + 1][i + 1]
									+ extendRawArray[j - 1][i + 1]
									+ extendRawArray[j + 1][i - 1]
									+ extendRawArray[j - 1][i - 1]) >> 2);
					//green at blue
					image->setG1(j, i,
							(extendRawArray[j + 1][i] + extendRawArray[j][i + 1]
									+ extendRawArray[j - 1][i]
									+ extendRawArray[j][i - 1]) >> 2);
					//blue at blue
					image->setB(j, i, extendRawArray[j][i]);
					break;
				case green2Code:
					//red at green2
					image->setR(j, i,
							(extendRawArray[j + 1][i] + extendRawArray[j - 1][i])
									>> 1);
					//green1 at green2
					image->setG1(j, i, extendRawArray[j][i]);
					//blue at green2
					image->setB(j, i,
							(extendRawArray[j][i + 1] + extendRawArray[j][i - 1])
									>> 1);
					break;
				}
			}
		}
		if (inputSettings.verbose)
            logger.log("bilinear demosaicing applied");
		blocked = false;
	}
}
void RAWImage::medianFilter(int maskSize) {

	if (!blocked) {
		//const int maskSize = 3;
		const int maskWidth = maskSize / 2;
		const int median = maskSize * maskSize / 2;
		const int maskPower = maskSize * maskSize;
		blocked = true;
		WorkingImage<arrayType, colorSize> * imageCopy =
				image->extendWorkingImage(maskWidth);

		int mask1[maskPower], mask2[maskPower];
		int mRG, mBG, temp;
		for (int j = 0; j < height; j++) {
			for (int i = 0; i < width; i++) {
				int maskIterator = 0;
				//arrayType* current;
				for (int m = -maskWidth; m <= maskWidth; m++)
					for (int n = -maskWidth; n <= maskWidth; n++) {
						mask1[maskIterator] = imageCopy->getR(j + m, i + n)
								- imageCopy->getG1(j + m, i + n);
						mask2[maskIterator] = imageCopy->getB(j + m, i + n)
								- imageCopy->getG1(j + m, i + n);
//                    current=source[j+m][i+n];
//                    mask1[maskIterator]=current[redCode]-current[green1Code];
//                    mask2[maskIterator]=current[blueCode]-current[green1Code];
						maskIterator++;

					}
				nth_element(mask1, mask1 + median, mask1 + maskPower);
				mRG = mask1[median];
				nth_element(mask2, mask2 + median, mask2 + maskPower);
				mBG = mask2[median];
				//red
				temp = imageCopy->getG1(j, i) + mRG;
				image->checkedSetR(j, i, temp);
				//green

				temp = (imageCopy->getR(j, i) + imageCopy->getB(j, i) - mRG
						- mBG) >> 1;
				image->checkedSetG1(j, i, temp);
				//blue
				temp = imageCopy->getG1(j, i) + mBG;
				image->checkedSetB(j, i, temp);

			}
		}
		delete imageCopy;
		if (inputSettings.verbose)
            logger.log("median filter applied");
		blocked = false;
	}
}
void RAWImage::impulseFilterCombined(int maskSize) {
	if (!blocked) {
		blocked = true;
		const int maskSize2 = 5;
		const int maskWidth = maskSize / 2;
		const int maskWidth2 = maskSize2 / 2;
		//const int maxMask = maskSize*maskSize/2+1;
		const int maskPower = maskSize2 * maskSize2;
		const int median = maskSize2 * maskSize2 / 2;
		int mask1[maskPower], mask2[maskPower];
		int temp5;
		int mRG, mBG;
		WorkingImage<arrayType, colorSize> * imageCopy =
				image->extendWorkingImage(maskWidth);
		for (int j = 0; j < height; j++) {
			for (int i = 0; i < width; i++) {
				int k1 = bayerFind(j, i);
				for (int k = 0; k < 3; k++) {
					int maskIterator = 0;
					if (k == green1Code) {
						if ((k1 & 1) == k)
							continue;
						//green value
						int formula = 1;
						for (int m = -maskWidth; m <= maskWidth; m++) {
							for (int n = -maskWidth + formula; n <= maskWidth;
									n += 2) {
								mask1[maskIterator++] = extendRawArray[j + m][i
										+ n];
							}
							formula = (!formula) & 1;
						}
						pair<int*, int*> minMax = minmax_element(mask1,
								mask1 + maskIterator);
						temp5 = image->getG1(j, i);
						if (temp5 > *minMax.second || temp5 < *minMax.first) {
							maskIterator = 0;
							for (int m = -maskWidth2; m <= maskWidth2; m++)
								for (int n = -maskWidth2; n <= maskWidth2;
										n++) {
									mask1[maskIterator] = imageCopy->getR(j + m,
											i + n)
											- imageCopy->getG1(j + m, i + n);
									mask2[maskIterator] = imageCopy->getB(j + m,
											i + n)
											- imageCopy->getG1(j + m, i + n);
									maskIterator++;

								}
							nth_element(mask1, mask1 + median,
									mask1 + maskPower);
							mRG = mask1[median];
							nth_element(mask2, mask2 + median,
									mask2 + maskPower);
							mBG = mask2[median];
							//green
							temp5 = (imageCopy->getR(j, i)
									+ imageCopy->getB(j, i) - mRG - mBG) >> 1;
							image->checkedSetG1(j, i, temp5);
						}
					} else {
						if (k1 == k)
							continue;
						int formula = (((k >> 1) ^ (k1 >> 1)) ^ maskWidth) & 1;
						int formula2 = (!((((k >> 1) & k1) ^ ((k1 >> 1) & k1))
								^ (maskWidth & 1))) & 1;
						for (int m = -maskWidth + formula; m <= maskWidth; m +=
								2)
							for (int n = -maskWidth + formula2; n <= maskWidth;
									n += 2) {
								mask1[maskIterator++] = extendRawArray[j + m][i
										+ n];
							}
						pair<int*, int*> minMax = minmax_element(mask1,
								mask1 + maskIterator);
						temp5 = image->get(j, i, k);
						if (temp5 > *minMax.second || temp5 < *minMax.first) {
							if (k == redCode) {
								maskIterator = 0;
								for (int m = -maskWidth2; m <= maskWidth2; m++)
									for (int n = -maskWidth2; n <= maskWidth2;
											n++) {
										mask1[maskIterator] = imageCopy->getR(
												j + m, i + n)
												- imageCopy->getG1(j + m,
														i + n);
										maskIterator++;
									}
								nth_element(mask1, mask1 + median,
										mask1 + maskPower);
								mRG = mask1[median];
								//red
								temp5 = imageCopy->getG1(j, i) + mRG;
								image->checkedSetR(j, i, temp5);
							} else {
								maskIterator = 0;
								for (int m = -maskWidth2; m <= maskWidth2; m++)
									for (int n = -maskWidth2; n <= maskWidth2;
											n++) {
										mask2[maskIterator] = imageCopy->getB(
												j + m, i + n)
												- imageCopy->getG1(j + m,
														i + n);
										maskIterator++;

									}
								nth_element(mask2, mask2 + median,
										mask2 + maskPower);
								mBG = mask2[median];
								//blue
								temp5 = imageCopy->getG1(j, i) + mBG;
								image->checkedSetB(j, i, temp5);
							}
						}
					}
				}
			}
		}
        if (inputSettings.verbose)
            logger.log("impulse filter with median mask applied");
		blocked = false;
	}
}
void RAWImage::impulseFilter(int maskSize) {
	if (!blocked) {
		blocked = true;
		const int maskWidth = maskSize / 2;
		const int maxMask = maskSize * maskSize / 2 + 1;
		int mask1[maxMask];
		int temp5;
		for (int j = 0; j < height; j++) {
			for (int i = 0; i < width; i++) {
				int k1 = bayerFind(j, i);
				for (int k = 0; k < 3; k++) {
					//memset(mask1,0, sizeof(int)*maxMask);
					int maskIterator = 0;
					if (k == green1Code) {
						if ((k1 & 1) == k)
							continue;
						//green value
						int formula = 1;
						for (int m = -maskWidth; m <= maskWidth; m++) {
							for (int n = -maskWidth + formula; n <= maskWidth;
									n += 2) {
								mask1[maskIterator++] = extendRawArray[j + m][i
										+ n];
							}
							formula = (!formula) & 1;
						}
						pair<int*, int*> minMax = minmax_element(mask1,
								mask1 + maskIterator);
						temp5 = image->getG1(j, i);
						if (temp5 > *minMax.second)
							image->setG1(j, i, *minMax.second);
						else if (temp5 < *minMax.first)
							image->setG1(j, i, *minMax.first);
					} else {
						if (k1 == k)
							continue;
						int formula = (((k >> 1) ^ (k1 >> 1)) ^ maskWidth) & 1;
						int formula2 = (!((((k >> 1) & k1) ^ ((k1 >> 1) & k1))
								^ (maskWidth & 1))) & 1;
						for (int m = -maskWidth + formula; m <= maskWidth; m +=
								2)
							for (int n = -maskWidth + formula2; n <= maskWidth;
									n += 2) {
								mask1[maskIterator++] = extendRawArray[j + m][i
										+ n];
							}
						pair<int*, int*> minMax = minmax_element(mask1,
								mask1 + maskIterator);
						temp5 = image->get(j, i, k);
						if (temp5 > *minMax.second)
							image->set(j, i, k, *minMax.second);
						else if (temp5 < *minMax.first)
							image->set(j, i, k, *minMax.first);
					}
				}
			}
		}
        if (inputSettings.verbose)
            logger.log("impulse filter applied");
		blocked = false;
	}
}
void RAWImage::impulseFilterBilinear(int maskSize) {
	if (!blocked) {
		blocked = true;
		const int maskWidth = maskSize / 2;
		const int maxMask = maskSize * maskSize / 2 + 1;
		int mask1[maxMask];
		int temp5;
		for (int j = 0; j < height; j++) {
			for (int i = 0; i < width; i++) {
				int k1 = bayerFind(j, i);
				for (int k = 0; k < 3; k++) {
					int maskIterator = 0;
					if (k == green1Code) {
						if ((k1 & 1) == k)
							continue;
						//green value
						int formula = 1;
						for (int m = -maskWidth; m <= maskWidth; m++) {
							for (int n = -maskWidth + formula; n <= maskWidth;
									n += 2) {
								mask1[maskIterator++] = extendRawArray[j + m][i
										+ n];
							}
							formula = (!formula) & 1;
						}
						pair<int*, int*> minMax = minmax_element(mask1,
								mask1 + maskIterator);
						temp5 = image->getG1(j, i);
						if (temp5 > *minMax.second || temp5 < *minMax.first) {
							image->setG1(j, i,
									(extendRawArray[j + 1][i]
											+ extendRawArray[j][i + 1]
											+ extendRawArray[j - 1][i]
											+ extendRawArray[j][i - 1]) >> 2);
						}
					} else {
						if (k1 == k)
							continue;
						int formula = (((k >> 1) ^ (k1 >> 1)) ^ maskWidth) & 1;
						int formula2 = (!((((k >> 1) & k1) ^ ((k1 >> 1) & k1))
								^ (maskWidth & 1))) & 1;
						for (int m = -maskWidth + formula; m <= maskWidth; m +=
								2)
							for (int n = -maskWidth + formula2; n <= maskWidth;
									n += 2) {
								mask1[maskIterator++] = extendRawArray[j + m][i
										+ n];
							}
						pair<int*, int*> minMax = minmax_element(mask1,
								mask1 + maskIterator);
						temp5 = image->get(j, i, k);
						if (temp5 > *minMax.second)
							image->set(j, i, k, *minMax.second);
						else if (temp5 < *minMax.first)
							image->set(j, i, k, *minMax.first);
					}
				}
			}
		}
        if (inputSettings.verbose)
            logger.log("impulse filter applied");
		blocked = false;
	}
}
void RAWImage::demosaic() {
	switch (inputSettings.demosaic) {
	case demosaicMethod::nearestNeighbor:
		nearestNeighborDemosaic_fast();
		break;
	case demosaicMethod::bilinear:
		bilinearDemosaic_fast();
		break;
	case demosaicMethod::simplegradients:
		simpleGradientsDemosaic();
		break;
	case demosaicMethod::VNG:
		VNGDemosaic();
		break;
	default:
		blocked = true;
		break;
	}
}
void RAWImage::impulseFilterPre(int maskSize) {
	if (!blocked) {
		const int maskWidth = maskSize / 2;
		const int maxMask = maskSize * maskSize / 2 + 1;
		RawWrapper * copy = sourceWrapper->copy();
		ushort** copyArray = copy->extendRawArray;
		int mask1[maxMask];
		int temp5;
		for (int j = 0; j < height; j++) {
			for (int i = 0; i < width; i++) {
				int k = bayerFind(j, i);
				int maskIterator = 0;
				if (k & 1) {
					int formula = 0;
					//green value
					for (int m = -maskWidth; m <= maskWidth; m++) {
						for (int n = -maskWidth + formula; n <= maskWidth; n +=
								2) {
							if (n == 0 && m == 0)
								continue;
							mask1[maskIterator++] = copyArray[j + m][i + n];
						}
						formula = (!formula) & 1;
					}
				} else {
					for (int m = -maskWidth + (maskWidth & 1); m <= maskWidth;
							m += 2)
						for (int n = -maskWidth + (maskWidth & 1);
								n <= maskWidth; n += 2) {
							if (n == 0 && m == 0)
								continue;
							mask1[maskIterator++] = copyArray[j + m][i + n];
						}
				}
				pair<int*, int*> minMax = minmax_element(mask1,
						mask1 + maskIterator);
				double sum = std::accumulate(mask1, mask1 + maskIterator, 0.0);
				double mean = sum / maskIterator;
				double diff[maskIterator];
				std::transform(mask1, mask1 + maskIterator, diff,
						std::bind2nd(std::minus<double>(), mean));
				double sq_sum = std::inner_product(diff, diff + maskIterator,
						diff, 0.0);
				double stdev = std::sqrt(sq_sum / maskIterator);
				temp5 = extendRawArray[j][i];
				if (temp5 > mean + 2 * stdev)
					extendRawArray[j][i] = *minMax.second;
				else if (temp5 < mean - 2 * stdev)
					extendRawArray[j][i] = *minMax.first;
			}
		}
		delete copy;
        if (inputSettings.verbose)
            logger.log("impulse prefilter applied");
		blocked = false;
	}
}
void RAWImage::medianFilterPre(int maskSize) {
	if (!blocked) {
		const int maskWidth = maskSize / 2;
		//const int median = maskSize*maskSize/2;
		const int maxMask = maskSize * maskSize / 2 + 1;
		RawWrapper * copy = sourceWrapper->copy();
		ushort** copyArray = copy->extendRawArray;
		int mask1[maxMask];
		for (int j = 0; j < height; j++) {
			for (int i = 0; i < width; i++) {
				int k = bayerFind(j, i);
				int maskIterator = 0;
				if (k & 1) {
					int formula = 0;
					//green value
					for (int m = -maskWidth; m <= maskWidth; m++) {
						for (int n = -maskWidth + formula; n <= maskWidth; n +=
								2) {
							mask1[maskIterator++] = copyArray[j + m][i + n];
						}
						formula = (!formula) & 1;
					}
				} else {
					for (int m = -maskWidth + (maskWidth & 1); m <= maskWidth;
							m += 2)
						for (int n = -maskWidth + (maskWidth & 1);
								n <= maskWidth; n += 2) {
							mask1[maskIterator++] = copyArray[j + m][i + n];
						}
				}
				int median = maskIterator / 2;
				nth_element(mask1, mask1 + median, mask1 + maskIterator);
				extendRawArray[j][i] = mask1[median];
			}
		}
		delete copy;
		if (inputSettings.verbose)
            logger.log("median prefilter applied");
		blocked = false;
	}
}
void RAWImage::nearestNeighborDemosaic_fast() {
	if (!blocked) {
		blocked = true;
		for (uint j = 0; j < height; j += 2) {
			for (uint i = 0; i < width; i += 2) {
				//set Red
				int temp = extendRawArray[j + colorOffsets[redCode].x][i
						+ colorOffsets[redCode].y];
				image->setR(j, i, temp);
				image->setR(j, i + 1, temp);
				image->setR(j + 1, i, temp);
				image->setR(j + 1, i + 1, temp);
				int off = colorOffsets[green1Code].x;
				temp = extendRawArray[j + colorOffsets[green1Code].x][i
						+ colorOffsets[green1Code].y];
				image->setG1(j + off, i, temp);
				image->setG1(j + off, i + 1, temp);
				temp = extendRawArray[j + colorOffsets[blueCode].x][i
						+ colorOffsets[blueCode].y];
				image->setB(j, i, temp);
				image->setB(j, i + 1, temp);
				image->setB(j + 1, i, temp);
				image->setB(j + 1, i + 1, temp);
				off = colorOffsets[green2Code].x;
				temp = extendRawArray[j + colorOffsets[green2Code].x][i
						+ colorOffsets[green2Code].y];
				image->setG1(j + off, i, temp);
				image->setG1(j + off, i + 1, temp);
			}
		}
		if (inputSettings.verbose)
            logger.log("nearest neighbor demosaic applied");
		blocked = false;
	}

}
void RAWImage::applyPrefilter() {
	switch (inputSettings.prefilter) {
	case prefilterType::statistic:
		impulseFilterPre(inputSettings.prefilterMask | 1);
		break;
	case prefilterType::median:
		medianFilterPre(inputSettings.prefilterMask | 1);
		break;
	case prefilterType::none:
		break;
	default:
		blocked = true;
		break;
	}

}
void RAWImage::applyPostfilter() {
	switch (inputSettings.postfilter) {
	case postfilterType::impulseFilter:
		impulseFilter(inputSettings.postfilterMask | 1);
		break;
	case postfilterType::impulseFilterWithMedian:
		impulseFilterCombined(inputSettings.postfilterMask | 1);
		break;
	case postfilterType::medianFilter:
		medianFilter(inputSettings.postfilterMask | 1);
		break;
	case postfilterType::impulseFilterBilinear:
		impulseFilterBilinear(inputSettings.postfilterMask | 1);
		break;
	case postfilterType::none:
		break;
	default:
		blocked = true;
		break;
	}

}
void RAWImage::spreadBits() {

	ushort* linearToLinear = new ushort[maxValue + 1];
	const int toValue = USHRT_MAX;
	if (maxValue == toValue)
		return;
	int toMax = sourceWrapper->maxValue();
	int i = 0;
	for (; i < toMax && i < maxValue + 1; i++) {
		linearToLinear[i] = round((toValue * i) / toMax);
	}
	for (; i < maxValue + 1; i++) {
		linearToLinear[i] = toValue;
	}
	for (uint i = 0; i < imageSize; i++) {
		singleMemoryBlock[i] = linearToLinear[singleMemoryBlock[i]];
	}
	delete[] linearToLinear;
}
int RAWImage::findMax() {
	maxValue = *max_element(singleMemoryBlock,
			singleMemoryBlock + imageSize);
    return maxValue;
}
void RAWImage::insertMax(int maxValue) {
	this->maxValue = maxValue;
}
void RAWImage::colorCorrection() {
	if (!blocked) {
		spreadBits();  //correct color depth range
		if (inputSettings.colorMatrix == colorMatrixType::none)
			return;
		blocked = true;
		auto matrix = sourceWrapper->getColorArray();
		for (uint j = 0; j < height; j++)
			for (uint i = 0; i < width; i++) {
				arrayType red = image->getR(j, i);
				arrayType green1 = image->getG1(j, i);
				arrayType blue = image->getB(j, i);
				double newRed = red * matrix[0][0] + green1 * matrix[0][1]
						+ blue * matrix[0][2];
				double newGreen1 = red * matrix[1][0] + green1 * matrix[1][1]
						+ blue * matrix[1][2];
				double newBlue = red * matrix[2][0] + green1 * matrix[2][1]
						+ blue * matrix[2][2];
				image->checkedSetR(j, i, newRed);
				image->checkedSetG1(j, i, newGreen1);
				image->checkedSetB(j, i, newBlue);
			}
		if (inputSettings.verbose)
            logger.log("color correction applied");
		blocked = false;
	}
}
void RAWImage::savePNG(OutputData &output) {
	if (!blocked) {
		blocked = true;
		PngReaderWriter writer;
		vector<char> out = writer.writePngToMemory(*image,inputSettings.precision);
		output.buffer = out;
		output.precision = precision;
        if (inputSettings.verbose)
            logger.log("image encoded as png");
        blocked = false;
	}
}

//getters/setters
int RAWImage::getPrecision() {
	return precision;
}
void RAWImage::free() {
	if (image != nullptr) {
		delete image;
		image = nullptr;
	}
}
RAWImage::~RAWImage() {
	if (inputSettings.verbose)
            logger.log("freeing up memory");
	free();
}
