/***************************************************************
 * Name:      GUIProjectMain.h
 * Purpose:   Defines Application Frame
 * Author:     ()
 * Created:   2014-06-30
 * Copyright:  ()
 * License:
 **************************************************************/

#ifndef GUIPROJECTMAIN_H
#define GUIPROJECTMAIN_H

#include <vector>
#include <fstream>
#include <SerializationObjects.h>

//(*Headers(GUIProjectFrame)
#include <wx/msgdlg.h>
#include <wx/spinctrl.h>
#include <wx/statbox.h>
#include <wx/filedlg.h>
#include <wx/choice.h>
#include <wx/gbsizer.h>
#include <wx/button.h>
#include <wx/frame.h>
//*)

class GUIProjectFrame: public wxFrame
{
    public:

        GUIProjectFrame(wxWindow* parent,wxWindowID id = -1);
        virtual ~GUIProjectFrame();

    private:

        //(*Handlers(GUIProjectFrame)
        void OnQuit(wxCommandEvent& event);
        void OnloadDngButtonClick(wxCommandEvent& event);
        //*)

        //(*Identifiers(GUIProjectFrame)
        static const long ID_BUTTON1;
        static const long ID_CHOICE2;
        static const long ID_CHOICE3;
        static const long ID_CHOICE1;
        static const long ID_CHOICE4;
        static const long ID_SPINCTRL3;
        static const long ID_SPINCTRL4;
        static const long ID_SPINCTRL2;
        static const long ID_SPINCTRL1;
        static const long ID_STATICBOX1;
        static const long ID_CHOICE5;
        static const long ID_MESSAGEDIALOG1;
        //*)

        //(*Declarations(GUIProjectFrame)
        wxChoice* Choice5;
        wxSpinCtrl* SpinCtrl4;
        wxSpinCtrl* SpinCtrl1;
        wxChoice* Choice3;
        wxFileDialog* FileDialog1;
        wxStaticBox* StaticBox1;
        wxSpinCtrl* SpinCtrl3;
        wxChoice* Choice4;
        wxSpinCtrl* SpinCtrl2;
        wxMessageDialog* MessageDialog1;
        wxButton* LoadDngButton;
        wxChoice* Choice1;
        wxChoice* Choice2;
        //*)
        std::vector<char> dngBuffer;
        DECLARE_EVENT_TABLE()
        int getDemosaic(wxChoice* Choice1);
        int getColorMatrix(wxChoice* Choice1);
        int getPrefilter(wxChoice* Choice1);
        int getPostfilter(wxChoice* Choice1);
        int getPrecision(wxChoice* Choice1);
};

#endif // GUIPROJECTMAIN_H
